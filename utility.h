#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

const char *replaceString(const char *msg, size_t size) {
  int i;
  char *result;
  size_t counter = 1;
  result = malloc((size * 5) + 1); // msg size (n) * max tillegg (5) + 1 for 0\ i malloc
  result[0] = '\0';                // marks last


  for (i = 0; i < (int)size; ++i) {

    if (msg[i] == '&') {
      strcat(result, "&amp;");
      counter += 5;

    } else if (msg[i] == '>') {
      strcat(result, "&gt;");
      counter += 4;

    } else if (msg[i] == '<') {
      strcat(result, "&lt;");
      counter += 4;

    } else {
      strncat(result, &msg[i], 1);
      counter += 1;
    }
  }
  result = realloc(result, counter);
  return result;
}